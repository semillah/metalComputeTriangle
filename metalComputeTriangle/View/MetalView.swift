//
//  MetalView.swift
//  metalComputeTriangle
//
//  Created by Raul on 1/13/24.
//

import Foundation
import SwiftUI     // Importing SwiftUI framework for building user interfaces in a declarative style.
import MetalKit    // Importing MetalKit framework for handling Metal rendering within UIKit or AppKit environments.




// Definition of MetalView structure conforming to UIViewRepresentable protocol.
// This allows integration of a UIKit view (MTKView in this case) within SwiftUI.
struct MetalView: UIViewRepresentable {
    
    
    
    // makeCoordinator function is modified to return an instance of Renderer.
    // Renderer is now serving as the coordinator for MetalView.
    // This sets up Renderer to handle the rendering tasks and delegate methods for the MTKView.
    func makeCoordinator() -> Renderer {
        
        Renderer(self)
        
    }
    
    
    
    
    
    // makeUIView function creates and returns a MTKView, the view for Metal content.
    // It sets the delegate of the MTKView to the coordinator (Renderer in this case).
    // This delegate setup allows Renderer to respond to Metal-specific rendering events.
    func makeUIView(context: UIViewRepresentableContext<MetalView>) -> MTKView {
        
        
        let mtkView = MTKView() // Instantiates a new MTKView, the primary view for Metal content.
        mtkView.delegate = context.coordinator // Sets the delegate of MTKView to the Renderer.
        // This means Renderer will handle the MTKViewDelegate methods.
        
        
        
        if let metalDevice = MTLCreateSystemDefaultDevice() {
            mtkView.device = metalDevice // Sets the Metal device for the view.
            // This is necessary for rendering any Metal content.
        }
        
        mtkView.preferredFramesPerSecond = 60 // Sets the frame rate of the view to 60 frames per second.
        // This determines how often the view's content is updated.
        
        
        mtkView.enableSetNeedsDisplay = true // Enables the view to redraw itself when setNeedsDisplay() is called.
        // This can be used for manual control of the redraw cycle.
        
        mtkView.framebufferOnly = false // Indicates that the contents of the framebuffer will be read by the CPU.
        // Setting to false allows for more flexibility, such as performing readback operations.
        
        
        mtkView.drawableSize = mtkView.frame.size // Sets the drawable size of the view to match its frame size.
        // This ensures that the rendering resolution matches the view size.
        
        
        
        mtkView.isPaused = false // This line ensures that the MTKView's internal loop is active.
        // By default, MTKView might pause its loop to conserve resources.
        // Setting isPaused to false keeps the rendering loop running,
        // allowing continuous rendering of frames, which is essential for animations or any dynamic content.
        
        mtkView.colorPixelFormat = .rgba8Unorm // Sets the pixel format for the color attachment of the view's framebuffer.
        // .rgba8Unorm is a common pixel format that represents a color using 8 bits each for red, green, blue, and alpha,
        // which are normalized to the range 0-1. This format is widely supported and offers a good balance between
        // quality and performance for most rendering tasks.
        
        
        
        return mtkView // Returns the configured MTKView to be used in the SwiftUI view hierarchy.
        
    }
    
    
    
    
    
    
    // updateUIView is called whenever the SwiftUI view's state changes and it needs to update the UIKit view.
    // This is where you update the MTKView in response to state changes in your SwiftUI app.
    // For example, you can adjust what and how things are rendered in response to user interactions.
    func updateUIView(_ uiView: UIViewType, context: UIViewRepresentableContext<MetalView>) {
        
        
        
    }
}
