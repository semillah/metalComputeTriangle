//
//  Renderer.swift
//  metalComputeTriangle
//
//  Created by Raul on 1/13/24.
//

import Foundation
import MetalKit

class Renderer: NSObject, MTKViewDelegate {
    struct Vertex {
        var position: SIMD4<Float>
        var color: SIMD4<Float>
    }

    var parent: MetalView
    var metalDevice: MTLDevice!
    var metalCommandQueue: MTLCommandQueue!
    var pipelineState: MTLRenderPipelineState!
    var vertices: [Vertex] = []
    var vertexBuffer: MTLBuffer!
    var time: Float = 0
    var computePipelineState: MTLComputePipelineState!

    init(_ parent: MetalView) {
        self.parent = parent
        super.init()

        guard let metalDevice = MTLCreateSystemDefaultDevice() else {
            fatalError("Metal is not supported on this device")
        }
        self.metalDevice = metalDevice
        self.metalCommandQueue = metalDevice.makeCommandQueue()
        setupRenderPipeline()
        setupComputePipeline()
        createVertices()
    }

    private func setupRenderPipeline() {
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        let library = metalDevice.makeDefaultLibrary()
        pipelineDescriptor.vertexFunction = library?.makeFunction(name: "vertexShader")
        pipelineDescriptor.fragmentFunction = library?.makeFunction(name: "fragmentShader")
        pipelineDescriptor.colorAttachments[0].pixelFormat = .rgba8Unorm

        do {
            pipelineState = try metalDevice.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch let error {
            fatalError("Failed to create render pipeline state: \(error)")
        }
    }

    private func setupComputePipeline() {
        let computePipelineDescriptor = MTLComputePipelineDescriptor()
        let library = metalDevice.makeDefaultLibrary()
        computePipelineDescriptor.computeFunction = library?.makeFunction(name: "computeShader")

        do {
            computePipelineState = try metalDevice.makeComputePipelineState(descriptor: computePipelineDescriptor, options: [], reflection: nil)
        } catch {
            fatalError("Failed to create compute pipeline state: \(error)")
        }

    }

    func update() {
        time += 0.01

        guard let commandBuffer = metalCommandQueue.makeCommandBuffer(),
              let computeEncoder = commandBuffer.makeComputeCommandEncoder() else {
            return
        }

        computeEncoder.setComputePipelineState(computePipelineState)
        computeEncoder.setBuffer(vertexBuffer, offset: 0, index: 0)
        computeEncoder.setBytes(&time, length: MemoryLayout<Float>.size, index: 1)

        let threadGroupCount = MTLSize(width: vertices.count, height: 1, depth: 1)
        let threadsPerThreadgroup = MTLSize(width: 1, height: 1, depth: 1)

        computeEncoder.dispatchThreads(threadGroupCount, threadsPerThreadgroup: threadsPerThreadgroup)
        computeEncoder.endEncoding()

        commandBuffer.commit()
    }

    private func createVertices() {
        vertices = [
            Vertex(position: SIMD4<Float>(0.0, 0.5, 0.0, 1.0), color: SIMD4<Float>(1, 0, 0, 1)),
            Vertex(position: SIMD4<Float>(-0.5, -0.5, 0.0, 1.0), color: SIMD4<Float>(0, 1, 0, 1)),
            Vertex(position: SIMD4<Float>(0.5, -0.5, 0.0, 1.0), color: SIMD4<Float>(0, 0, 1, 1)),
        ]
        vertexBuffer = metalDevice.makeBuffer(bytes: vertices, length: vertices.count * MemoryLayout<Vertex>.stride, options: .storageModeShared)
    }

    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        // Handle view size changes if necessary
    }

    func draw(in view: MTKView) {
        update()

        guard let drawable = view.currentDrawable,
              let commandBuffer = metalCommandQueue.makeCommandBuffer(),
              let renderPassDescriptor = view.currentRenderPassDescriptor else {
            return
        }

        let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)
        renderEncoder?.setViewport(MTLViewport(originX: 0, originY: 0, width: Double(view.drawableSize.width), height: Double(view.drawableSize.height), znear: 0, zfar: 1))
        renderEncoder?.setRenderPipelineState(pipelineState)
        renderEncoder?.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        renderEncoder?.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: vertices.count)
        renderEncoder?.endEncoding()

        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
}
