//
//  ContentView.swift
//  metalComputeTriangle
//
//  Created by Raul on 1/13/24.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    var body: some View {
        VStack {
            MetalView() // Embedding the MetalView within the SwiftUI view hierarchy.
            // This is where the Metal content will be displayed.
            
            
        }
        .padding()
    }
}
