//
//  metalComputeTriangleApp.swift
//  metalComputeTriangle
//
//  Created by Raul on 1/13/24.
//

import SwiftUI

@main
struct metalComputeTriangleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
