//
//  perlinNoise.metal
//  metalComputeTriangle
//
//  Created by Raul on 1/13/24.
//

#include <metal_stdlib>
using namespace metal;


// Permutation table replicated from the C++ version
constant uint8_t permutation[256] = {
    151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};

// Helper functions translated from C++
inline float fade(float t) {
    return t * t * t * (t * (t * 6 - 15) + 10);
}

inline float lerp(float a, float b, float t) {
    return a + (b - a) * t;
}

inline float grad(int hash, float x, float y, float z) {
    int h = hash & 15;
    float u = h < 8 ? x : y;
    float v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

// The noise3D function translated from C++
float noise3D(float x, float y, float z) {
    int X = int(floor(x)) & 255;
    int Y = int(floor(y)) & 255;
    int Z = int(floor(z)) & 255;
    
    x -= floor(x);
    y -= floor(y);
    z -= floor(z);
    
    float u = fade(x);
    float v = fade(y);
    float w = fade(z);
    
    int A = permutation[X] + Y;
    int AA = permutation[A] + Z;
    int AB = permutation[A + 1] + Z;
    int B = permutation[X + 1] + Y;
    int BA = permutation[B] + Z;
    int BB = permutation[B + 1] + Z;
    
    return lerp(lerp(lerp(grad(permutation[AA], x, y, z),
                          grad(permutation[BA], x - 1, y, z), u),
                     lerp(grad(permutation[AB], x, y - 1, z),
                          grad(permutation[BB], x - 1, y - 1, z), u), v),
                lerp(lerp(grad(permutation[AA + 1], x, y, z - 1),
                          grad(permutation[BA + 1], x - 1, y, z - 1), u),
                     lerp(grad(permutation[AB + 1], x, y - 1, z - 1),
                          grad(permutation[BB + 1], x - 1, y - 1, z - 1), u), v), w);
}


// Octave Noise for 3D
float octaveNoise3D(float x, float y, float z, int octaves, float persistence) {
    float total = 0.0;
    float frequency = 1.0;
    float amplitude = 1.0;
    float maxValue = 0.0;  // Used for normalizing result to [-1, 1]
    
    for(int i = 0; i < octaves; i++) {
        total += noise3D(x * frequency, y * frequency, z * frequency) * amplitude;
        
        maxValue += amplitude;
        
        amplitude *= persistence;
        frequency *= 2;
    }
    
    return total / maxValue;
}



// 1D Perlin Noise function
float noise1D(float x) {
    // Using fixed default values for Y and Z as in the C++ implementation
    const float defaultY = 0.12345;
    const float defaultZ = 0.34567;
    
    // Directly using the 3D noise function with fixed Y and Z values
    return noise3D(x, defaultY, defaultZ);
}

// Octave Noise for 1D
float octaveNoise1D(float x, int octaves, float persistence) {
    float total = 0.0;
    float frequency = 1.0;
    float amplitude = 1.0;
    float maxValue = 0.0;  // Used for normalizing result to [-1, 1]
    
    for(int i = 0; i < octaves; i++) {
        total += noise1D(x * frequency) * amplitude;
        
        maxValue += amplitude;
        
        amplitude *= persistence;
        frequency *= 2;
    }
    
    return total / maxValue;
}







// 2D Perlin Noise function
float noise2D(float x, float y) {
    // Using a fixed default value for Z as in the C++ implementation
    const float defaultZ = 0.34567;
    
    // Utilizing the 3D noise function with a fixed Z value
    return noise3D(x, y, defaultZ);
}

// Octave Noise for 2D
float octaveNoise2D(float x, float y, int octaves, float persistence) {
    float total = 0.0;
    float frequency = 1.00;
    float amplitude = 1.0;
    float maxValue = 0.0;  // Used for normalizing result to [-1, 1]
    
    for(int i = 0; i < octaves; i++) {
        total += noise2D(x * frequency, y * frequency) * amplitude;
        
        maxValue += amplitude;
        
        amplitude *= persistence;
        frequency *= 2;
    }
    
    return total / maxValue;
}
