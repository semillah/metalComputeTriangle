//
//  Shaders.metal
//  metalComputeTriangle
//
//  Created by Raul on 1/13/24.
//

#include <metal_stdlib>
using namespace metal;

// Define the structure of the vertex data that will be passed from Swift code
struct Vertex {
    float4 position [[position]];
    float4 color;
};

// Vertex shader function
vertex Vertex vertexShader(uint vertexID [[ vertex_id ]],
                           const device Vertex* vertices [[ buffer(0) ]]) {
    return vertices[vertexID];
}

// Fragment shader function
fragment float4 fragmentShader(Vertex in [[stage_in]]) {
    return in.color; // Use the color passed from the vertex shader
}

// Compute shader function
kernel void computeShader(device Vertex* vertices [[ buffer(0) ]],
                          constant float& time [[ buffer(1) ]],
                          uint id [[ thread_position_in_grid ]]) {
    // Move all vertices upward
    vertices[id].position.y += 0.001; // You can adjust the value as needed for the speed of movement
}

